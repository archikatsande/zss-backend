package com.book.management.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionResponse {
    private LocalDateTime updated;
    private String responseCode;
    private String responseDescription;
    private String reference;
    private String debitReference;
}
