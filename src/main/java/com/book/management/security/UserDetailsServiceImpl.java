package com.book.management.security;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static Collection<? extends GrantedAuthority> getAuthorities(){

        Collection<SimpleGrantedAuthority> roles = new ArrayList<>();

        SimpleGrantedAuthority role = new SimpleGrantedAuthority("USER");

        roles.add(role);

        return roles;

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Collection<? extends GrantedAuthority> roles = getAuthorities();
        return new User("zss", "zss",true,true,true,true,roles);

    }
}
