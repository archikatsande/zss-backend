package com.book.management.exception;

public class ApiRequestException extends RuntimeException{


    public ApiRequestException(String message) {
        super(message);
    }

}
