package com.book.management.repository;

import com.book.management.model.Book;
import com.book.management.model.Category;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends PagingAndSortingRepository<Book,Long> {
    @Override
    Optional<Book> findById(Long id);
    List<Book> findAllByCategory(Category category);

}
