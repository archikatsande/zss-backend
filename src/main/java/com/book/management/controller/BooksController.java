package com.book.management.controller;

import com.book.management.model.Book;
import com.book.management.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.List;

@Controller
public class BooksController {
    private BookService bookService;
    @Autowired
    public BooksController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/book/{categoryId}")
    public ResponseEntity<Book> createNewBook(@Valid @RequestBody Book book, @PathVariable Long categoryId) {
        Book newBook=this.bookService.createNewBook(book, categoryId);
            return new ResponseEntity<>(newBook, HttpStatus.CREATED);

    }
    @GetMapping("/books/page/{pageNumber}")
    public ResponseEntity<List<Book>> getBooks(@PathVariable Integer pageNumber){
        List<Book> books =this.bookService.getBooks(pageNumber);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<List<Book>> getBooksByCategory(@PathVariable Long categoryId){
        List<Book> books =this.bookService.getBooksByBookCategory(categoryId);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

}
