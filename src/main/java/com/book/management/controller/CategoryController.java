package com.book.management.controller;

import com.book.management.model.Category;
import com.book.management.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/category")
    public ResponseEntity<Map<String ,Object>> createNewCategory(@Valid @RequestBody Category category){
        this.categoryService.createNewCategory(category);
        Map<String ,Object> response = this.categoryService.createResponse(category);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
