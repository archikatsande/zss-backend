package com.book.management.service;

import com.book.management.exception.ApiRequestException;
import com.book.management.model.Book;
import com.book.management.model.Category;
import com.book.management.repository.BookRepository;
import com.book.management.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class BookService {
    private BookRepository bookRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public BookService(BookRepository bookRepository, CategoryRepository categoryRepository) {
        this.bookRepository = bookRepository;
        this.categoryRepository = categoryRepository;
    }

    public Book createNewBook(Book book, Long categoryId) throws ApiRequestException {
        Optional<Category> category =this.categoryRepository.findById(categoryId);
        if(category.isPresent()) {
            book.setCategory(category.get());
            return this.bookRepository.save(book);
        }else {
            throw new ApiRequestException("Category not Found");
        }
    }

    public List<Book> getBooksByBookCategory(Long categoryId) {
        Optional<Category> category = this.categoryRepository.findById(categoryId);
        if(category.isPresent()) {
            return this.bookRepository.findAllByCategory(category.get());
        }
        else {
            return Collections.emptyList();
        }
    }

    public List<Book> getBooks(Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, 5, Sort.by("title"));
        return this.bookRepository.findAll(pageable).getContent();
    }
}
