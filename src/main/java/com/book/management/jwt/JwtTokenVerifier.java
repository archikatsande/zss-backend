package com.book.management.jwt;

import com.book.management.security.UserDetailsServiceImpl;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtTokenVerifier extends OncePerRequestFilter {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request
            , HttpServletResponse response
            , FilterChain filterChain) throws ServletException, IOException {

  String authorizationRequest =request.getHeader("Authorization");
        if(authorizationRequest==null || !authorizationRequest.startsWith("Bearer ") ){
      filterChain.doFilter(request,response);
      return;
  }
        String token =authorizationRequest.replace("Bearer ","");
        try {
            if(token.equals("9ca3d5ed-dc04-4700-8dd6-7d60c3cdf0fa")){
            String username="zss";
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);

      }

        }catch (JwtException e){
            throw new IllegalStateException(String.format("Token %s cannot be trusted",token));
        }

       filterChain.doFilter(request,response);
    }
}
