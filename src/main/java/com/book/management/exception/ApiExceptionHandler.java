package com.book.management.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleRequestException(ApiRequestException e){
        ApiExceptionResponse apiExceptionResponse = new ApiExceptionResponse(e.getMessage(),HttpStatus.BAD_REQUEST, ZonedDateTime.now(ZoneOffset.UTC));
        return new ResponseEntity<>(apiExceptionResponse,HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleBadRequestException(MethodArgumentNotValidException e){
        Map<String, String> errors = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach(error -> {
            String errorMessage = error.getDefaultMessage();
            errors.put("message",errorMessage);
        });
        ApiExceptionResponse apiExceptionResponse = new ApiExceptionResponse(errors.get("message"),HttpStatus.BAD_REQUEST, ZonedDateTime.now(ZoneOffset.UTC));
        return new ResponseEntity<>(apiExceptionResponse,HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handleMismatchType(MethodArgumentTypeMismatchException e) {
        String name = e.getName();
        String type = Objects.requireNonNull(e.getRequiredType()).getSimpleName();
        Object value = e.getValue();
        String message = String.format("%s should be a valid '%s' and you provided '%s'", name, type, value);
        ApiExceptionResponse apiExceptionResponse = new ApiExceptionResponse(message,HttpStatus.BAD_REQUEST, ZonedDateTime.now(ZoneOffset.UTC));
        return new ResponseEntity<>(apiExceptionResponse,HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<Object> handleMockRequestException(TransactionException e){
        ApiExceptionResponse apiExceptionResponse = new ApiExceptionResponse(e.getMessage(),HttpStatus.BAD_REQUEST, ZonedDateTime.now(ZoneOffset.UTC));
        return new ResponseEntity<>(apiExceptionResponse,HttpStatus.BAD_REQUEST);
    }

}
