package com.book.management.service;

import com.book.management.model.Category;
import com.book.management.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class CategoryService {
    private CategoryRepository categoryRepository;
    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public boolean createNewCategory(Category category) {
        this.categoryRepository.save(category);
        return true;

    }

    public Map<String ,Object> createResponse(Category category) {
        Map<String ,Object>  map = new HashMap<>();
        map.put("message","Category Created");
        map.put("name",category.getTitle());
        map.put("status","successful");
        map.put("timestamp", ZonedDateTime.now(ZoneOffset.UTC));
        return map;
    }
}
