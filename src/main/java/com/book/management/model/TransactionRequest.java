package com.book.management.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {
    @JsonProperty
    private String type;
    @JsonProperty
    private String extendedType;
    @JsonProperty
    private String reference;
    @JsonProperty
    private String narration;
    @JsonProperty
    private Map<String, Object> additionalData;
    @JsonProperty
    private Double amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern="yyyy-MM-dd'T'HH:mm:ss.SSSz")
    private LocalDateTime created;
    @JsonProperty
    private Card card;

}
