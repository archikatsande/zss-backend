package com.book.management.configuration;

import com.book.management.model.TransactionResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.*;

@Configuration
public class TransactionResponseConfig {
    @Bean("approvedResponse")
    TransactionResponse approvedResponse(){
        return TransactionResponse
                .builder()
                .responseCode("000")
                .responseDescription("Approved")
                .debitReference(UUID.randomUUID().toString())
                .updated(LocalDateTime.now())
                .build();
    }
    @Bean("declinedResponse")
    TransactionResponse declinedResponse(){
        return TransactionResponse
                .builder()
                .responseCode("005")
                .responseDescription("Do Not Honour")
                .debitReference(UUID.randomUUID().toString())
                .updated(LocalDateTime.now())
                .build();
    }
    @Bean("malfunctionResponse")
    TransactionResponse malfunctionResponse(){
        return TransactionResponse
                .builder()
                .responseCode("001")
                .responseDescription("Contact Issuer")
                .debitReference(UUID.randomUUID().toString())
                .updated(LocalDateTime.now())
                .build();
    }
    @Bean("exceptionResponse")
    TransactionResponse exceptionResponse(){
        return TransactionResponse
                .builder()
                .responseCode("096")
                .responseDescription("System Malfunction")
                .debitReference(UUID.randomUUID().toString())
                .updated(LocalDateTime.now())
                .build();
    }
    @Bean("invalidRequestResponse")
    TransactionResponse invalidRequestResponse(){
        return TransactionResponse
                .builder()
                .responseCode("012")
                .responseDescription("Invalid Transaction")
                .debitReference(UUID.randomUUID().toString())
                .updated(LocalDateTime.now())
                .build();

    }
    @Bean
    List<TransactionResponse> transactionResponseList(){
        return new ArrayList<>();
    }

    @Bean
    Random random(){
        return new Random();
    }

    @Bean("types")
    List<String> types(){
        return new ArrayList<>(Collections.singletonList("PURCHASE"));
    }
    @Bean("extendedTypes")
    List<String> extendedTypes(){
        return new ArrayList<>(Collections.singletonList("NONE"));
    }
}
