package com.book.management.service;

import com.book.management.exception.TransactionException;
import com.book.management.model.TransactionRequest;
import com.book.management.model.TransactionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class TransactionService {

    @Qualifier("approvedResponse")
    private final TransactionResponse approvedResponse;

    @Qualifier("declinedResponse")
    private final TransactionResponse declinedResponse;

    @Qualifier("malfunctionResponse")
    private final TransactionResponse malfunctionResponse;

    @Qualifier("exceptionResponse")
    private final TransactionResponse exceptionResponse;

    @Qualifier("invalidRequestResponse")
    private final TransactionResponse invalidRequestResponse;

    private final List<TransactionResponse> transactionResponses;

    @Qualifier("types")
    private final List<String> types;

    @Qualifier("extendedTypes")
    private final List<String> extendedTypes;

    private final Random random;

    @Autowired
    public TransactionService(TransactionResponse approvedResponse, TransactionResponse declinedResponse, TransactionResponse malfunctionResponse, TransactionResponse exceptionResponse, TransactionResponse invalidRequestResponse,List<TransactionResponse> transactionResponses,Random random,List<String> types,List<String> extendedTypes) {
        this.approvedResponse = approvedResponse;
        this.declinedResponse = declinedResponse;
        this.malfunctionResponse = malfunctionResponse;
        this.exceptionResponse = exceptionResponse;
        this.invalidRequestResponse = invalidRequestResponse;
        this.transactionResponses =transactionResponses;
        this.random =random;
        this.types=types;
        this.extendedTypes =extendedTypes;
    }

    public TransactionResponse processTransaction(TransactionRequest transactionRequest) {
        if(this.types.contains(transactionRequest.getType())&& this.extendedTypes.contains(transactionRequest.getExtendedType())){
            switch (transactionRequest.getCard().getId()) {
                case "1234560000000001":
                    this.approvedResponse.setReference(transactionRequest.getReference());
                    return this.approvedResponse;
                case "1234560000000002":
                    this.declinedResponse.setReference(transactionRequest.getReference());
                    return this.declinedResponse;
                case "1234560000000003":
                    this.declinedResponse.setReference(transactionRequest.getReference());
                    this.approvedResponse.setReference(transactionRequest.getReference());
                    this.transactionResponses.add(this.declinedResponse);
                    this.transactionResponses.add(this.approvedResponse);
                    return transactionResponses.get(this.random.nextInt(transactionResponses.size()));
                case "1234560000000004":
                    throw new TransactionException(this.exceptionResponse.getResponseDescription());
                default:
                    this.malfunctionResponse.setReference(transactionRequest.getReference());
                    return this.malfunctionResponse;
            }
        }else{
             this.invalidRequestResponse.setReference(transactionRequest.getReference());
             return this.invalidRequestResponse;
        }
    }
}