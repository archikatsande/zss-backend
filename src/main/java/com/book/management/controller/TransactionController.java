package com.book.management.controller;

import com.book.management.model.TransactionRequest;
import com.book.management.model.TransactionResponse;
import com.book.management.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class TransactionController {

    private final TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/api/transaction")
    public ResponseEntity<TransactionResponse> makeTransaction(@RequestBody TransactionRequest transactionRequest){
       return new ResponseEntity<>(this.transactionService.processTransaction(transactionRequest), HttpStatus.OK);
    }
}
